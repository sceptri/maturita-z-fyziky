# Vypracování maturitních otázek

Tento dokument má sloužit jako sloučení několika zdrojů dohromady, a to zvláště encyklopedie fyzika.jreichl.com a radek.jandora.sweb.cz, avšak také i dalších zdrojů.

Jakékoliv připomínky můžete psát do *issues* nebo jako *merge request*

Pro čtení stačí stáhnout soubor **fyzika.pdf**

## Compilování ze zdrojového kódu
Nutné mít nainstalované: `latexmk,pdflatex` 

Dále při compilování je nutné doplnit argument: `--shell-escape`

> Sceptri 