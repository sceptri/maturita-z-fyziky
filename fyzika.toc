\babel@toc {czech}{}
\contentsline {chapter}{\numberline {1}Kinematika}{8}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Z\IeC {\'a}kladn\IeC {\'\i } pojmy}{8}{section.1.1}% 
\contentsline {section}{\numberline {1.2}D\IeC {\v e}len\IeC {\'\i } pohyb\IeC {\r u}}{8}{section.1.2}% 
\contentsline {subsection}{\numberline {1.2.1}Podle tvaru trajektorie}{8}{subsection.1.2.1}% 
\contentsline {subsection}{\numberline {1.2.2}Podle ura\IeC {\v z}en\IeC {\'e} vzd\IeC {\'a}lenosti za jednotku \IeC {\v c}asu}{8}{subsection.1.2.2}% 
\contentsline {paragraph}{Note}{8}{section*.2}% 
\contentsline {section}{\numberline {1.3}Z\IeC {\'a}kladn\IeC {\'\i } veli\IeC {\v c}iny}{9}{section.1.3}% 
\contentsline {section}{\numberline {1.4}Grafy pohyb\IeC {\r u}}{9}{section.1.4}% 
\contentsline {subsection}{\numberline {1.4.1}Pohyb rovnom\IeC {\v e}rn\IeC {\'y}}{9}{subsection.1.4.1}% 
\contentsline {subsection}{\numberline {1.4.2}Pohyb rovnom\IeC {\v e}rn\IeC {\v e} zrychlen\IeC {\'y}}{10}{subsection.1.4.2}% 
\contentsline {section}{\numberline {1.5}Skl\IeC {\'a}d\IeC {\'a}n\IeC {\'\i } pohyb\IeC {\r u}}{11}{section.1.5}% 
\contentsline {section}{\numberline {1.6}Rovnom\IeC {\v e}rn\IeC {\'y} pohyb po kru\IeC {\v z}nici}{11}{section.1.6}% 
\contentsline {section}{\numberline {1.7}Rovnom\IeC {\v e}rn\IeC {\v e} zrychlen\IeC {\'y} pohyb po kru\IeC {\v z}nici}{12}{section.1.7}% 
\contentsline {chapter}{\numberline {2}Dynamika}{13}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Z\IeC {\'a}kladn\IeC {\'\i } pojmy}{13}{section.2.1}% 
\contentsline {section}{\numberline {2.2}Newtonovy pohybov\IeC {\'e} z\IeC {\'a}kony}{13}{section.2.2}% 
\contentsline {subsection}{\numberline {2.2.1}Prvn\IeC {\'\i } Newton\IeC {\r u}v pohybov\IeC {\'y} z\IeC {\'a}kon}{13}{subsection.2.2.1}% 
\contentsline {paragraph}{Definice}{13}{section*.6}% 
\contentsline {subsubsection}{Pojmy}{14}{section*.7}% 
\contentsline {subsection}{\numberline {2.2.2}Druh\IeC {\'y} Newton\IeC {\r u}v pohybov\IeC {\'y} z\IeC {\'a}kon}{14}{subsection.2.2.2}% 
\contentsline {paragraph}{Definice}{14}{section*.8}% 
\contentsline {subsubsection}{Origini\IeC {\'a}ln\IeC {\'\i } definice s\IeC {\'\i }ly}{14}{section*.9}% 
\contentsline {subsection}{\numberline {2.2.3}T\IeC {\v r}et\IeC {\'\i } Newton\IeC {\r u}v pohybov\IeC {\'y} z\IeC {\'a}kon}{14}{subsection.2.2.3}% 
\contentsline {paragraph}{Definice}{14}{section*.10}% 
\contentsline {paragraph}{Note}{14}{section*.11}% 
\contentsline {paragraph}{U\IeC {\v z}it\IeC {\'\i }}{14}{section*.12}% 
\contentsline {section}{\numberline {2.3}Z\IeC {\'a}kon zachov\IeC {\'a}n\IeC {\'\i } hybnosti}{14}{section.2.3}% 
\contentsline {paragraph}{Definice}{14}{section*.13}% 
\contentsline {section}{\numberline {2.4}Skl\IeC {\'a}d\IeC {\'a}n\IeC {\'\i } sil}{14}{section.2.4}% 
\contentsline {subsubsection}{Vektorov\IeC {\'y} sou\IeC {\v c}et}{15}{section*.14}% 
\contentsline {section}{\numberline {2.5}Setrva\IeC {\v c}n\IeC {\'e} s\IeC {\'\i }ly}{15}{section.2.5}% 
\contentsline {subsection}{\numberline {2.5.1}P\IeC {\v r}\IeC {\'\i }klad: Kuli\IeC {\v c}ka ve vag\IeC {\'o}nu}{15}{subsection.2.5.1}% 
\contentsline {subsection}{\numberline {2.5.2}P\IeC {\v r}\IeC {\'\i }klad: V\IeC {\'y}tah}{16}{subsection.2.5.2}% 
\contentsline {section}{\numberline {2.6}Dost\IeC {\v r}ediv\IeC {\'a} s\IeC {\'\i }la}{16}{section.2.6}% 
\contentsline {section}{\numberline {2.7}Smykov\IeC {\'e} t\IeC {\v r}en\IeC {\'\i } a valiv\IeC {\'y} odpor}{16}{section.2.7}% 
\contentsline {subsection}{\numberline {2.7.1}Smykov\IeC {\'e} t\IeC {\v r}en\IeC {\'\i }}{16}{subsection.2.7.1}% 
\contentsline {subsection}{\numberline {2.7.2}Valiv\IeC {\'y} odpor}{17}{subsection.2.7.2}% 
\contentsline {chapter}{\numberline {3}Mechanick\IeC {\'a} pr\IeC {\'a}ce a energie}{18}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Z\IeC {\'a}kladn\IeC {\'\i } pojmy}{18}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Potenci\IeC {\'a}ln\IeC {\'\i } energie}{18}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}Potenci\IeC {\'a}ln\IeC {\'\i } energie pru\IeC {\v z}nosti}{18}{subsection.3.2.1}% 
\contentsline {section}{\numberline {3.3}Kinetick\IeC {\'a} energie}{19}{section.3.3}% 
\contentsline {section}{\numberline {3.4}Z\IeC {\'a}kon zachov\IeC {\'a}n\IeC {\'\i } mechanick\IeC {\'e} energie}{19}{section.3.4}% 
\contentsline {section}{\numberline {3.5}R\IeC {\'a}zy t\IeC {\v e}les}{19}{section.3.5}% 
\contentsline {subsection}{\numberline {3.5.1}Dokonale pru\IeC {\v z}n\IeC {\'y} r\IeC {\'a}z}{19}{subsection.3.5.1}% 
\contentsline {subsection}{\numberline {3.5.2}Dokonale nepru\IeC {\v z}n\IeC {\'y} r\IeC {\'a}z}{19}{subsection.3.5.2}% 
\contentsline {subsection}{\numberline {3.5.3}Nedokonale pru\IeC {\v z}n\IeC {\'y} r\IeC {\'a}z}{19}{subsection.3.5.3}% 
\contentsline {section}{\numberline {3.6}V\IeC {\'y}kon, p\IeC {\v r}\IeC {\'\i }kon, \IeC {\'u}\IeC {\v c}innost}{20}{section.3.6}% 
\contentsline {chapter}{\numberline {4}Mechanika tuh\IeC {\'e}ho t\IeC {\v e}lesa}{21}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Z\IeC {\'a}kladn\IeC {\'\i } pojmy}{21}{section.4.1}% 
\contentsline {section}{\numberline {4.2}\IeC {\'U}hlov\IeC {\'e} veli\IeC {\v c}iny}{21}{section.4.2}% 
\contentsline {subsection}{\numberline {4.2.1}Moment s\IeC {\'\i }ly}{21}{subsection.4.2.1}% 
\contentsline {paragraph}{Sm\IeC {\v e}r momentu s\IeC {\'\i }ly}{21}{section*.18}% 
\contentsline {subsection}{\numberline {4.2.2}Momentov\IeC {\'a} v\IeC {\v e}ta}{22}{subsection.4.2.2}% 
\contentsline {section}{\numberline {4.3}Skl\IeC {\'a}d\IeC {\'a}n\IeC {\'\i } sil}{22}{section.4.3}% 
\contentsline {section}{\numberline {4.4}T\IeC {\v e}\IeC {\v z}i\IeC {\v s}t\IeC {\v e}}{23}{section.4.4}% 
\contentsline {section}{\numberline {4.5}Rovnov\IeC {\'a}\IeC {\v z}n\IeC {\'e} polohy}{23}{section.4.5}% 
\contentsline {section}{\numberline {4.6}Moment setrva\IeC {\v c}nosti}{23}{section.4.6}% 
\contentsline {subsection}{\numberline {4.6.1}Steinerova v\IeC {\v e}ta}{23}{subsection.4.6.1}% 
\contentsline {chapter}{\numberline {5}Mechanika kapalin a plyn\IeC {\r u}}{24}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Z\IeC {\'a}kladn\IeC {\'\i } pojmy}{24}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Hydrostatika}{24}{section.5.2}% 
\contentsline {subsection}{\numberline {5.2.1}Tlak}{24}{subsection.5.2.1}% 
\contentsline {paragraph}{Definice}{24}{section*.21}% 
\contentsline {section}{\numberline {5.3}Hydrodynamika}{26}{section.5.3}% 
\contentsline {subsection}{\numberline {5.3.1}Z\IeC {\'a}kladn\IeC {\'\i } pojmy}{26}{subsection.5.3.1}% 
\contentsline {subsection}{\numberline {5.3.2}Rovnice kontinuity}{26}{subsection.5.3.2}% 
\contentsline {subsection}{\numberline {5.3.3}Bernoulliho rovnice}{26}{subsection.5.3.3}% 
\contentsline {paragraph}{Hydrodynamick\IeC {\'y} paradox}{27}{section*.24}% 
\contentsline {subsection}{\numberline {5.3.4}Obt\IeC {\'e}k\IeC {\'a}n\IeC {\'\i } t\IeC {\v e}les}{27}{subsection.5.3.4}% 
\contentsline {chapter}{\numberline {6}Gravita\IeC {\v c}n\IeC {\'\i } pole}{28}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Z\IeC {\'a}kladn\IeC {\'\i } pojmy}{28}{section.6.1}% 
\contentsline {section}{\numberline {6.2}Newton\IeC {\r u}v gravita\IeC {\v c}n\IeC {\'\i } z\IeC {\'a}kon}{29}{section.6.2}% 
\contentsline {subsection}{\numberline {6.2.1}Intenzita gravita\IeC {\v c}n\IeC {\'\i }ho pole}{29}{subsection.6.2.1}% 
\contentsline {subsection}{\numberline {6.2.2}Zrychlen\IeC {\'\i } gravita\IeC {\v c}n\IeC {\'\i }ho pole}{29}{subsection.6.2.2}% 
\contentsline {subsection}{\numberline {6.2.3}T\IeC {\'\i }hov\IeC {\'e} zrychlen\IeC {\'\i }}{29}{subsection.6.2.3}% 
\contentsline {section}{\numberline {6.3}Pohyby v t\IeC {\'\i }hov\IeC {\'e}m poli}{29}{section.6.3}% 
\contentsline {section}{\numberline {6.4}Pohyby v radi\IeC {\'a}ln\IeC {\'\i }m poli}{29}{section.6.4}% 
\contentsline {subsection}{\numberline {6.4.1}Kosmick\IeC {\'e} rychlosti}{30}{subsection.6.4.1}% 
\contentsline {subsection}{\numberline {6.4.2}Keplerovy z\IeC {\'a}kony}{30}{subsection.6.4.2}% 
\contentsline {chapter}{\numberline {7}Kmit\IeC {\'a}n\IeC {\'\i }}{32}{chapter.7}% 
\contentsline {section}{\numberline {7.1}Z\IeC {\'a}kladn\IeC {\'\i } pojmy}{32}{section.7.1}% 
\contentsline {section}{\numberline {7.2}Kinematika kmitav\IeC {\'e}ho pohybu}{32}{section.7.2}% 
\contentsline {subsection}{\numberline {7.2.1}Pojmy}{32}{subsection.7.2.1}% 
\contentsline {subsection}{\numberline {7.2.2}Rovnice harmonick\IeC {\'e}ho kmit\IeC {\'a}n\IeC {\'\i }}{33}{subsection.7.2.2}% 
\contentsline {subsection}{\numberline {7.2.3}Rychlost a zrychlen\IeC {\'\i } harmonick\IeC {\'e}ho kmit\IeC {\'a}n\IeC {\'\i }}{33}{subsection.7.2.3}% 
\contentsline {section}{\numberline {7.3}Slo\IeC {\v z}en\IeC {\'e} kmit\IeC {\'a}n\IeC {\'\i }}{34}{section.7.3}% 
\contentsline {subsection}{\numberline {7.3.1}P\IeC {\v r}\IeC {\'\i }pady}{34}{subsection.7.3.1}% 
\contentsline {section}{\numberline {7.4}Dynamika kmitav\IeC {\'e}ho pohybu}{34}{section.7.4}% 
\contentsline {subsection}{\numberline {7.4.1}T\IeC {\v e}leso na pru\IeC {\v z}in\IeC {\v e}}{34}{subsection.7.4.1}% 
\contentsline {subsection}{\numberline {7.4.2}Matematick\IeC {\'e} kyvadlo}{35}{subsection.7.4.2}% 
\contentsline {section}{\numberline {7.5}Energie kmit\IeC {\'a}n\IeC {\'\i }}{36}{section.7.5}% 
\contentsline {section}{\numberline {7.6}Typy kmit\IeC {\'a}n\IeC {\'\i }}{37}{section.7.6}% 
\contentsline {chapter}{\numberline {8}Vln\IeC {\v e}n\IeC {\'\i }}{38}{chapter.8}% 
\contentsline {section}{\numberline {8.1}Z\IeC {\'a}kladn\IeC {\'\i } pojmy}{38}{section.8.1}% 
\contentsline {section}{\numberline {8.2}Rovnice vlny}{39}{section.8.2}% 
\contentsline {section}{\numberline {8.3}Interference vln\IeC {\v e}n\IeC {\'\i }}{39}{section.8.3}% 
\contentsline {paragraph}{Definice}{39}{section*.31}% 
\contentsline {section}{\numberline {8.4}Odraz vln\IeC {\v e}n\IeC {\'\i }}{40}{section.8.4}% 
\contentsline {subsection}{\numberline {8.4.1}Odraz vln\IeC {\v e}n\IeC {\'\i } od pevn\IeC {\'e}ho konce}{40}{subsection.8.4.1}% 
\contentsline {subsection}{\numberline {8.4.2}Odraz vln\IeC {\v e}n\IeC {\'\i } od voln\IeC {\'e}ho konce}{40}{subsection.8.4.2}% 
\contentsline {section}{\numberline {8.5}Stojat\IeC {\'e} vln\IeC {\v e}n\IeC {\'\i }}{41}{section.8.5}% 
\contentsline {subsection}{\numberline {8.5.1}Chv\IeC {\v e}n\IeC {\'\i }}{41}{subsection.8.5.1}% 
\contentsline {section}{\numberline {8.6}Huygens\IeC {\r u}v princip}{42}{section.8.6}% 
\contentsline {section}{\numberline {8.7}Odraz, lom a ohyb vln\IeC {\v e}n\IeC {\'\i }}{42}{section.8.7}% 
\contentsline {subsection}{\numberline {8.7.1}Odraz}{42}{subsection.8.7.1}% 
\contentsline {subsection}{\numberline {8.7.2}Lom}{43}{subsection.8.7.2}% 
\contentsline {subsection}{\numberline {8.7.3}Ohyb}{44}{subsection.8.7.3}% 
\contentsline {section}{\numberline {8.8}Polarizace}{44}{section.8.8}% 
\contentsline {section}{\numberline {8.9}Zvuk}{45}{section.8.9}% 
\contentsline {chapter}{\numberline {9}Zobrazen\IeC {\'\i } odrazem}{46}{chapter.9}% 
\contentsline {section}{\numberline {9.1}Z\IeC {\'a}kladn\IeC {\'\i } informace}{46}{section.9.1}% 
\contentsline {section}{\numberline {9.2}Rovinn\IeC {\'e} zrcadlo}{47}{section.9.2}% 
\contentsline {section}{\numberline {9.3}Kulov\IeC {\'e} zrcadlo}{47}{section.9.3}% 
\contentsline {subsection}{\numberline {9.3.1}Zobrazovac\IeC {\'\i } rovnice kulov\IeC {\'e}ho zrcadla}{48}{subsection.9.3.1}% 
\contentsline {subsection}{\numberline {9.3.2}Vypukl\IeC {\'e} zrcadlo}{49}{subsection.9.3.2}% 
\contentsline {subsection}{\numberline {9.3.3}Dut\IeC {\'e} zrcadlo}{49}{subsection.9.3.3}% 
\contentsline {subsection}{\numberline {9.3.4}P\IeC {\v r}\IeC {\'\i }\IeC {\v c}n\IeC {\'e} zv\IeC {\v e}t\IeC {\v s}en\IeC {\'\i }}{49}{subsection.9.3.4}% 
\contentsline {section}{\numberline {9.4}Vyu\IeC {\v z}it\IeC {\'\i } zobrazen\IeC {\'\i } odrazem}{50}{section.9.4}% 
\contentsline {chapter}{\numberline {10}Zobrazov\IeC {\'a}n\IeC {\'\i } lomem}{51}{chapter.10}% 
\contentsline {section}{\numberline {10.1}Z\IeC {\'a}kladn\IeC {\'\i } informace}{51}{section.10.1}% 
\contentsline {section}{\numberline {10.2}\IeC {\'U}pln\IeC {\'y} odraz}{52}{section.10.2}% 
\contentsline {section}{\numberline {10.3}Ohniskov\IeC {\'a} vzd\IeC {\'a}lenost}{52}{section.10.3}% 
\contentsline {section}{\numberline {10.4}Zobrazovac\IeC {\'\i } rovnice \IeC {\v c}o\IeC {\v c}ky}{53}{section.10.4}% 
\contentsline {section}{\numberline {10.5}V\IeC {\'y}zna\IeC {\v c}n\IeC {\'e} paprsky}{53}{section.10.5}% 
\contentsline {paragraph}{Pozn\IeC {\'a}mka:}{54}{section*.48}% 
\contentsline {section}{\numberline {10.6}P\IeC {\v r}\IeC {\'\i }\IeC {\v c}n\IeC {\'e} zv\IeC {\v e}t\IeC {\v s}en\IeC {\'\i }}{54}{section.10.6}% 
\contentsline {section}{\numberline {10.7}Zorn\IeC {\'y} \IeC {\'u}hel a \IeC {\'u}hlov\IeC {\'e} zv\IeC {\v e}t\IeC {\v s}en\IeC {\'\i }}{54}{section.10.7}% 
\contentsline {subsection}{\numberline {10.7.1}Zorn\IeC {\'y} \IeC {\'u}hel}{54}{subsection.10.7.1}% 
\contentsline {subsection}{\numberline {10.7.2}\IeC {\'U}hlov\IeC {\'e} zv\IeC {\v e}t\IeC {\v s}en\IeC {\'\i }}{54}{subsection.10.7.2}% 
\contentsline {section}{\numberline {10.8}Optick\IeC {\'e} p\IeC {\v r}\IeC {\'\i }stroje}{54}{section.10.8}% 
\contentsline {subsection}{\numberline {10.8.1}Lupa}{54}{subsection.10.8.1}% 
\contentsline {subsection}{\numberline {10.8.2}Mikroskop}{55}{subsection.10.8.2}% 
\contentsline {subsection}{\numberline {10.8.3}Dalekohled}{56}{subsection.10.8.3}% 
\contentsline {subsubsection}{Kepler\IeC {\r u}v dalekohled}{57}{section*.51}% 
\contentsline {subsection}{\numberline {10.8.4}Galile\IeC {\r u}v dalekohled}{57}{subsection.10.8.4}% 
\contentsline {subsection}{\numberline {10.8.5}Newton\IeC {\r u}v dalekohled}{58}{subsection.10.8.5}% 
\contentsline {section}{\numberline {10.9}Oko}{58}{section.10.9}% 
\contentsline {subsection}{\numberline {10.9.1}Vady oka}{59}{subsection.10.9.1}% 
\contentsline {chapter}{\numberline {11}Vlnov\IeC {\'e} vlastnosti sv\IeC {\v e}tla}{60}{chapter.11}% 
\contentsline {section}{\numberline {11.1}Z\IeC {\'a}kladn\IeC {\'\i } informace}{60}{section.11.1}% 
\contentsline {section}{\numberline {11.2}Disperze sv\IeC {\v e}tla}{60}{section.11.2}% 
\contentsline {section}{\numberline {11.3}Interference}{61}{section.11.3}% 
\contentsline {subsection}{\numberline {11.3.1}Koherence}{61}{subsection.11.3.1}% 
\contentsline {subsection}{\numberline {11.3.2}Interference na tenk\IeC {\'e} vrstv\IeC {\v e}}{61}{subsection.11.3.2}% 
\contentsline {paragraph}{Ot\IeC {\'a}\IeC {\v c}en\IeC {\'\i } f\IeC {\'a}ze na rozhran\IeC {\'\i }}{61}{section*.58}% 
\contentsline {subsection}{\numberline {11.3.3}Newtonova skla}{63}{subsection.11.3.3}% 
\contentsline {section}{\numberline {11.4}Ohyb sv\IeC {\v e}tla na optick\IeC {\'e} m\IeC {\v r}\IeC {\'\i }\IeC {\v z}ce}{63}{section.11.4}% 
\contentsline {paragraph}{Nult\IeC {\'e} maximum}{64}{section*.62}% 
\contentsline {section}{\numberline {11.5}Polarizace sv\IeC {\v e}tla}{64}{section.11.5}% 
\contentsline {section}{\numberline {11.6}Fotometrie}{65}{section.11.6}% 
\contentsline {chapter}{\numberline {12}Tepeln\IeC {\'e} jevy v l\IeC {\'a}tk\IeC {\'a}ch}{66}{chapter.12}% 
\contentsline {section}{\numberline {12.1}Z\IeC {\'a}kladn\IeC {\'\i } informace}{66}{section.12.1}% 
\contentsline {section}{\numberline {12.2}Teplota a jej\IeC {\'\i } m\IeC {\v e}\IeC {\v r}en\IeC {\'\i }}{67}{section.12.2}% 
\contentsline {section}{\numberline {12.3}Vnit\IeC {\v r}n\IeC {\'\i } energie}{68}{section.12.3}% 
\contentsline {section}{\numberline {12.4}Teplo}{68}{section.12.4}% 
\contentsline {subsection}{\numberline {12.4.1}Prvn\IeC {\'\i } termodynamick\IeC {\'y} z\IeC {\'a}kon}{68}{subsection.12.4.1}% 
\contentsline {section}{\numberline {12.5}Tepeln\IeC {\'a} kapacita}{68}{section.12.5}% 
\contentsline {subsection}{\numberline {12.5.1}Tepeln\IeC {\'a} kapacita t\IeC {\v e}lesa}{68}{subsection.12.5.1}% 
\contentsline {subsection}{\numberline {12.5.2}M\IeC {\v e}rn\IeC {\'a} tepeln\IeC {\'a} kapacita}{68}{subsection.12.5.2}% 
\contentsline {subsection}{\numberline {12.5.3}Kalorimetrick\IeC {\'a} rovnice}{69}{subsection.12.5.3}% 
\contentsline {chapter}{\numberline {13}Zm\IeC {\v e}ny skupenstv\IeC {\'\i } l\IeC {\'a}tek}{70}{chapter.13}% 
\contentsline {section}{\numberline {13.1}F\IeC {\'a}zov\IeC {\'y} diagram}{70}{section.13.1}% 
\contentsline {section}{\numberline {13.2}T\IeC {\'a}n\IeC {\'\i } a tuhnut\IeC {\'\i }}{71}{section.13.2}% 
\contentsline {section}{\numberline {13.3}Sublimace a desublimace}{72}{section.13.3}% 
\contentsline {section}{\numberline {13.4}Vypa\IeC {\v r}ov\IeC {\'a}n\IeC {\'\i } a kondenzace}{72}{section.13.4}% 
\contentsline {paragraph}{(M\IeC {\v e}rn\IeC {\'e}) skupensk\IeC {\'e} teplo varu}{72}{section*.67}% 
\contentsline {section}{\numberline {13.5}Vlhkost vzduchu}{72}{section.13.5}% 
\contentsline {subsection}{\numberline {13.5.1}Absolutn\IeC {\'\i } vlhkost vzduchu}{72}{subsection.13.5.1}% 
\contentsline {subsection}{\numberline {13.5.2}Relativn\IeC {\'\i } vlhkost vzduchu}{73}{subsection.13.5.2}% 
\contentsline {subsection}{\numberline {13.5.3}Dal\IeC {\v s}\IeC {\'\i } informace}{73}{subsection.13.5.3}% 
\contentsline {subsubsection}{Rosn\IeC {\'y} bod}{73}{section*.68}% 
\contentsline {subsubsection}{Vlhkom\IeC {\v e}ry}{73}{section*.69}% 
\contentsline {chapter}{\numberline {14}Struktura a vlastnosti plyn\IeC {\r u}}{74}{chapter.14}% 
\contentsline {section}{\numberline {14.1}Z\IeC {\'a}kladn\IeC {\'\i } informace}{74}{section.14.1}% 
\contentsline {section}{\numberline {14.2}Stavov\IeC {\'a} rovnice ide\IeC {\'a}ln\IeC {\'\i }ho plynu}{74}{section.14.2}% 
\contentsline {subsection}{\numberline {14.2.1}St\IeC {\v r}edn\IeC {\'\i } kvadratick\IeC {\'a} rychlost}{74}{subsection.14.2.1}% 
\contentsline {subsubsection}{Z\IeC {\'a}vislost st\IeC {\v r}edn\IeC {\'\i } kvadratick\IeC {\'e} rychlosti na teplot\IeC {\v e}}{75}{section*.70}% 
\contentsline {subsection}{\numberline {14.2.2}Stavov\IeC {\'a} rovnice ide\IeC {\'a}ln\IeC {\'\i }ho plynu}{75}{subsection.14.2.2}% 
\contentsline {section}{\numberline {14.3}D\IeC {\v e}je v plynech}{76}{section.14.3}% 
\contentsline {subsection}{\numberline {14.3.1}Izotermick\IeC {\'y} d\IeC {\v e}j}{76}{subsection.14.3.1}% 
\contentsline {subsection}{\numberline {14.3.2}Izochorick\IeC {\'y} d\IeC {\v e}j}{76}{subsection.14.3.2}% 
\contentsline {subsection}{\numberline {14.3.3}Izobarick\IeC {\'y} d\IeC {\v e}j}{77}{subsection.14.3.3}% 
\contentsline {subsection}{\numberline {14.3.4}Adiabatick\IeC {\'y} d\IeC {\v e}j}{77}{subsection.14.3.4}% 
\contentsline {section}{\numberline {14.4}Kruhov\IeC {\'y} d\IeC {\v e}j}{78}{section.14.4}% 
\contentsline {subsection}{\numberline {14.4.1}Prvn\IeC {\'\i } termodynamick\IeC {\'y} z\IeC {\'a}kon}{79}{subsection.14.4.1}% 
\contentsline {subsection}{\numberline {14.4.2}Druh\IeC {\'y} termodynamick\IeC {\'y} z\IeC {\'a}kon}{79}{subsection.14.4.2}% 
\contentsline {chapter}{\numberline {15}Struktura a vlastnosti kapalin}{80}{chapter.15}% 
\contentsline {section}{\numberline {15.1}Z\IeC {\'a}kladn\IeC {\'\i } informace}{80}{section.15.1}% 
\contentsline {section}{\numberline {15.2}Povrchov\IeC {\'a} vrstva kapaliny}{80}{section.15.2}% 
\contentsline {subsection}{\numberline {15.2.1}Potenci\IeC {\'a}ln\IeC {\'\i } energie povrchov\IeC {\'e} vrstvy}{81}{subsection.15.2.1}% 
\contentsline {section}{\numberline {15.3}Povrchov\IeC {\'e} nap\IeC {\v e}t\IeC {\'\i }}{81}{section.15.3}% 
\contentsline {section}{\numberline {15.4}Jevy na rozhran\IeC {\'\i } pevn\IeC {\'e}ho t\IeC {\v e}lesa a kapaliny}{82}{section.15.4}% 
\contentsline {subsection}{\numberline {15.4.1}Kapil\IeC {\'a}rn\IeC {\'\i } tlak}{83}{subsection.15.4.1}% 
\contentsline {subsection}{\numberline {15.4.2}Kapka}{84}{subsection.15.4.2}% 
\contentsline {section}{\numberline {15.5}Kapilarita}{84}{section.15.5}% 
\contentsline {subsection}{\numberline {15.5.1}Vyu\IeC {\v z}it\IeC {\'\i } kapilarity}{85}{subsection.15.5.1}% 
\contentsline {section}{\numberline {15.6}Teplotn\IeC {\'\i } rozta\IeC {\v z}nost vody}{85}{section.15.6}% 
\contentsline {subsection}{\numberline {15.6.1}Anom\IeC {\'a}lie vody}{85}{subsection.15.6.1}% 
\contentsline {chapter}{\numberline {16}Struktura a vlastnosti pevn\IeC {\'y}ch l\IeC {\'a}tek}{86}{chapter.16}% 
\contentsline {section}{\numberline {16.1}Z\IeC {\'a}kladn\IeC {\'\i } informace}{86}{section.16.1}% 
\contentsline {section}{\numberline {16.2}Krystalick\IeC {\'a} m\IeC {\v r}\IeC {\'\i }\IeC {\v z}ka}{87}{section.16.2}% 
\contentsline {subsection}{\numberline {16.2.1}Poruchy krystalick\IeC {\'e} m\IeC {\v r}\IeC {\'\i }\IeC {\v z}ky}{88}{subsection.16.2.1}% 
\contentsline {section}{\numberline {16.3}Vazby v krystalech}{88}{section.16.3}% 
\contentsline {section}{\numberline {16.4}Deformace pevn\IeC {\'e}ho t\IeC {\v e}lesa}{89}{section.16.4}% 
\contentsline {subsection}{\numberline {16.4.1}Deformace podle p\IeC {\r u}sob\IeC {\'\i }c\IeC {\'\i }ch sil}{89}{subsection.16.4.1}% 
\contentsline {section}{\numberline {16.5}S\IeC {\'\i }la pru\IeC {\v z}nosti, norm\IeC {\'a}lov\IeC {\'e} nap\IeC {\v e}t\IeC {\'\i }}{90}{section.16.5}% 
\contentsline {section}{\numberline {16.6}Hook\IeC {\r u}v z\IeC {\'a}kon pro pru\IeC {\v z}nou deformaci}{90}{section.16.6}% 
\contentsline {subsection}{\numberline {16.6.1}K\IeC {\v r}ivka deformace}{91}{subsection.16.6.1}% 
\contentsline {section}{\numberline {16.7}Teplotn\IeC {\'\i } rozta\IeC {\v z}nost pevn\IeC {\'y}ch l\IeC {\'a}tek}{91}{section.16.7}% 
\contentsline {chapter}{\numberline {17}Elektrick\IeC {\'e} pole}{92}{chapter.17}% 
\contentsline {section}{\numberline {17.1}Z\IeC {\'a}kladn\IeC {\'\i } infomace, elektrick\IeC {\'y} n\IeC {\'a}boj}{92}{section.17.1}% 
\contentsline {section}{\numberline {17.2}Coulomb\IeC {\r u}v z\IeC {\'a}kon}{93}{section.17.2}% 
\contentsline {section}{\numberline {17.3}Elektrick\IeC {\'e} pole}{94}{section.17.3}% 
\contentsline {section}{\numberline {17.4}Pr\IeC {\'a}ce v elektrick\IeC {\'e}m poli, elektrick\IeC {\'e} nap\IeC {\v e}t\IeC {\'\i }}{95}{section.17.4}% 
\contentsline {section}{\numberline {17.5}Potenci\IeC {\'a}ln\IeC {\'\i } energie a elektrick\IeC {\'y} potenci\IeC {\'a}l}{96}{section.17.5}% 
\contentsline {subsection}{\numberline {17.5.1}Ekvipotenci\IeC {\'a}ln\IeC {\'\i } plochy}{96}{subsection.17.5.1}% 
\contentsline {section}{\numberline {17.6}Elektrostatick\IeC {\'e} pole nabit\IeC {\'e}ho vodiv\IeC {\'e}ho t\IeC {\v e}lesa}{96}{section.17.6}% 
\contentsline {section}{\numberline {17.7}T\IeC {\v e}lesa v elektrostatick\IeC {\'e}m poli}{97}{section.17.7}% 
\contentsline {subsection}{\numberline {17.7.1}Vodi\IeC {\v c} v elektrostatick\IeC {\'e}m poli}{97}{subsection.17.7.1}% 
\contentsline {subsection}{\numberline {17.7.2}Izolant v elektrostatick\IeC {\'e}m poli}{98}{subsection.17.7.2}% 
\contentsline {section}{\numberline {17.8}Kapacita vodi\IeC {\v c}e, kondenz\IeC {\'a}tor}{99}{section.17.8}% 
\contentsline {subsection}{\numberline {17.8.1}Spojov\IeC {\'a}n\IeC {\'\i } kondenz\IeC {\'a}tor\IeC {\r u}}{100}{subsection.17.8.1}% 
\contentsline {subsection}{\numberline {17.8.2}Energie kondenz\IeC {\'a}toru}{101}{subsection.17.8.2}% 
\contentsline {chapter}{\numberline {18}Obvod stejnosm\IeC {\v e}rn\IeC {\'e}ho elektrick\IeC {\'e}ho proudu}{102}{chapter.18}% 
\contentsline {section}{\numberline {18.1}Elektrick\IeC {\'y} proud}{102}{section.18.1}% 
\contentsline {paragraph}{Definice:}{102}{section*.102}% 
\contentsline {section}{\numberline {18.2}Elektrick\IeC {\'y} zdroj}{103}{section.18.2}% 
\contentsline {subsection}{\numberline {18.2.1}Druhy elektrick\IeC {\'y}ch zdroj\IeC {\r u}}{104}{subsection.18.2.1}% 
\contentsline {section}{\numberline {18.3}Elektrick\IeC {\'y} proud ve vodi\IeC {\v c}\IeC {\'\i }ch}{104}{section.18.3}% 
\contentsline {subsection}{\numberline {18.3.1}Z\IeC {\'a}vislost odporu na tvaru a materi\IeC {\'a}lu vodi\IeC {\v c}e}{105}{subsection.18.3.1}% 
\contentsline {subsection}{\numberline {18.3.2}Z\IeC {\'a}vislost odporu na teplot\IeC {\v e}}{105}{subsection.18.3.2}% 
\contentsline {subsection}{\numberline {18.3.3}Rozsahy m\IeC {\v e}\IeC {\v r}\IeC {\'\i }c\IeC {\'\i }ch p\IeC {\v r}\IeC {\'\i }stroj\IeC {\r u}}{105}{subsection.18.3.3}% 
\contentsline {subsection}{\numberline {18.3.4}Rezistory s prom\IeC {\v e}nn\IeC {\'y}m odporem}{106}{subsection.18.3.4}% 
\contentsline {subsection}{\numberline {18.3.5}Zna\IeC {\v c}ky elektrick\IeC {\'y}ch spot\IeC {\v r}ebi\IeC {\v c}\IeC {\r u}}{106}{subsection.18.3.5}% 
\contentsline {section}{\numberline {18.4}Ohm\IeC {\r u}v z\IeC {\'a}kon pro uzav\IeC {\v r}en\IeC {\'y} obvod}{107}{section.18.4}% 
\contentsline {section}{\numberline {18.5}Spojov\IeC {\'a}n\IeC {\'\i } rezistor\IeC {\r u}}{107}{section.18.5}% 
\contentsline {section}{\numberline {18.6}Kirchhoffovy z\IeC {\'a}kony}{107}{section.18.6}% 
\contentsline {section}{\numberline {18.7}Pr\IeC {\'a}ce a v\IeC {\'y}kon stejnosm\IeC {\v e}rn\IeC {\'e}ho proudu}{108}{section.18.7}% 
\contentsline {subsection}{\numberline {18.7.1}\IeC {\'U}\IeC {\v c}innost obvodu}{108}{subsection.18.7.1}% 
\contentsline {chapter}{\numberline {19}Elektrick\IeC {\'y} proud v l\IeC {\'a}tk\IeC {\'a}ch}{109}{chapter.19}% 
\contentsline {section}{\numberline {19.1}Elektrick\IeC {\'y} proud v polovodi\IeC {\v c}\IeC {\'\i }ch}{109}{section.19.1}% 
\contentsline {subsection}{\numberline {19.1.1}Vlastn\IeC {\'\i } vodivost}{109}{subsection.19.1.1}% 
\contentsline {subsection}{\numberline {19.1.2}Nevlastn\IeC {\'\i } vodivost}{110}{subsection.19.1.2}% 
\contentsline {subsection}{\numberline {19.1.3}P\IeC {\v r}echod PN}{111}{subsection.19.1.3}% 
\contentsline {section}{\numberline {19.2}Elektrick\IeC {\'y} proud v kapalin\IeC {\'a}ch}{112}{section.19.2}% 
\contentsline {subsection}{\numberline {19.2.1}Z\IeC {\'a}kladn\IeC {\'\i } informace}{112}{subsection.19.2.1}% 
\contentsline {subsection}{\numberline {19.2.2}Faradayovy z\IeC {\'a}kony}{113}{subsection.19.2.2}% 
\contentsline {subsubsection}{Faraday\IeC {\r u}v z\IeC {\'a}kon pro elektrol\IeC {\'y}zu}{113}{section*.116}% 
\contentsline {subsubsection}{Prvn\IeC {\'\i } Faraday\IeC {\r u}v z\IeC {\'a}kon}{113}{section*.117}% 
\contentsline {subsubsection}{Druh\IeC {\'y} Faraday\IeC {\r u}v z\IeC {\'a}kon}{114}{section*.118}% 
\contentsline {subsection}{\numberline {19.2.3}Galvanick\IeC {\'e} \IeC {\v c}l\IeC {\'a}nky}{114}{subsection.19.2.3}% 
\contentsline {section}{\numberline {19.3}Elektrick\IeC {\'y} proud v plynech}{114}{section.19.3}% 
\contentsline {subsection}{\numberline {19.3.1}V\IeC {\'y}boje za atmosferick\IeC {\'e}ho a sn\IeC {\'\i }\IeC {\v z}en\IeC {\'e}ho tlaku}{115}{subsection.19.3.1}% 
\contentsline {chapter}{\numberline {20}Obvod st\IeC {\v r}\IeC {\'\i }dav\IeC {\'e}ho proudu}{117}{chapter.20}% 
\contentsline {section}{\numberline {20.1}Z\IeC {\'a}kladn\IeC {\'\i } informace}{117}{section.20.1}% 
\contentsline {section}{\numberline {20.2}Jednoduch\IeC {\'e} obvody st\IeC {\v r}\IeC {\'\i }dav\IeC {\'e}ho proudu}{118}{section.20.2}% 
\contentsline {subsection}{\numberline {20.2.1}Obvod s rezistorem}{118}{subsection.20.2.1}% 
\contentsline {subsection}{\numberline {20.2.2}Obvod s c\IeC {\'\i }vkou}{119}{subsection.20.2.2}% 
\contentsline {subsection}{\numberline {20.2.3}Obvod s kondenz\IeC {\'a}torem}{119}{subsection.20.2.3}% 
\contentsline {section}{\numberline {20.3}S\IeC {\'e}riov\IeC {\'y} RLC obvod}{120}{section.20.3}% 
\contentsline {section}{\numberline {20.4}Paraleln\IeC {\'\i } RLC obvod}{121}{section.20.4}% 
\contentsline {section}{\numberline {20.5}V\IeC {\'y}kon}{122}{section.20.5}% 
\contentsline {subsection}{\numberline {20.5.1}Obvod s rezistorem}{122}{subsection.20.5.1}% 
\contentsline {subsection}{\numberline {20.5.2}Obvod s impedanc\IeC {\'\i }}{123}{subsection.20.5.2}% 
\contentsline {section}{\numberline {20.6}Technick\IeC {\'e} vyu\IeC {\v z}it\IeC {\'\i } st\IeC {\v r}\IeC {\'\i }dav\IeC {\'e}ho proudu}{123}{section.20.6}% 
\contentsline {chapter}{\numberline {21}Stacion\IeC {\'a}rn\IeC {\'\i } magnetick\IeC {\'e} pole}{126}{chapter.21}% 
\contentsline {section}{\numberline {21.1}Z\IeC {\'a}kladn\IeC {\'\i } informace}{126}{section.21.1}% 
\contentsline {section}{\numberline {21.2}Magnetick\IeC {\'a} s\IeC {\'\i }la}{127}{section.21.2}% 
\contentsline {section}{\numberline {21.3}Magnetick\IeC {\'a} indukce}{128}{section.21.3}% 
\contentsline {subsection}{\numberline {21.3.1}Amp\IeC {\'e}r\IeC {\r u}v z\IeC {\'a}kon}{129}{subsection.21.3.1}% 
\contentsline {section}{\numberline {21.4}Nabit\IeC {\'a} \IeC {\v c}\IeC {\'a}stice v magnetick\IeC {\'e}m poli}{130}{section.21.4}% 
\contentsline {section}{\numberline {21.5}Magnetick\IeC {\'e} vlastnosti objekt\IeC {\r u}}{131}{section.21.5}% 
\contentsline {subsection}{\numberline {21.5.1}Magnetick\IeC {\'e} vlastnosti atom\IeC {\r u}}{131}{subsection.21.5.1}% 
\contentsline {subsection}{\numberline {21.5.2}Magnetick\IeC {\'e} vlastnosti l\IeC {\'a}tek}{131}{subsection.21.5.2}% 
\contentsline {subsection}{\numberline {21.5.3}Magnetick\IeC {\'a} hystereze}{132}{subsection.21.5.3}% 
\contentsline {section}{\numberline {21.6}Technick\IeC {\'e} vyu\IeC {\v z}it\IeC {\'\i }}{133}{section.21.6}% 
\contentsline {chapter}{\numberline {22}Nestacion\IeC {\'a}rn\IeC {\'\i } magnetick\IeC {\'e} pole}{134}{chapter.22}% 
\contentsline {section}{\numberline {22.1}Elektromagnetick\IeC {\'a} indukce}{134}{section.22.1}% 
\contentsline {section}{\numberline {22.2}Magnetick\IeC {\'y} induk\IeC {\v c}n\IeC {\'\i } tok}{134}{section.22.2}% 
\contentsline {section}{\numberline {22.3}Faraday\IeC {\r u}v z\IeC {\'a}kon elektromagnetick\IeC {\'e} indukce}{135}{section.22.3}% 
\contentsline {section}{\numberline {22.4}Indukovan\IeC {\'y} proud}{136}{section.22.4}% 
\contentsline {section}{\numberline {22.5}Vlastn\IeC {\'\i } indukce}{136}{section.22.5}% 
\contentsline {section}{\numberline {22.6}P\IeC {\v r}echodn\IeC {\'y} d\IeC {\v e}j}{137}{section.22.6}% 
\contentsline {section}{\numberline {22.7}Energie magnetick\IeC {\'e}ho pole c\IeC {\'\i }vky}{138}{section.22.7}% 
\contentsline {chapter}{\numberline {23}Z\IeC {\'a}kladn\IeC {\'\i } poznatky kvantov\IeC {\'e} fyziky}{139}{chapter.23}% 
\contentsline {section}{\numberline {23.1}Kvantov\IeC {\'a} hypot\IeC {\'e}za}{139}{section.23.1}% 
\contentsline {section}{\numberline {23.2}Vn\IeC {\v e}j\IeC {\v s}\IeC {\'\i } fotoelektrick\IeC {\'y} jev}{139}{section.23.2}% 
\contentsline {section}{\numberline {23.3}Dualismus vlna-\IeC {\v c}\IeC {\'a}stice}{140}{section.23.3}% 
\contentsline {subsection}{\numberline {23.3.1}Compton\IeC {\r u}v jev}{140}{subsection.23.3.1}% 
\contentsline {section}{\numberline {23.4}De Broglieho hypot\IeC {\'e}za}{141}{section.23.4}% 
\contentsline {subsection}{\numberline {23.4.1}Davisson-Germerovy experimenty}{142}{subsection.23.4.1}% 
\contentsline {section}{\numberline {23.5}Spektrum atomu}{143}{section.23.5}% 
\contentsline {subsection}{\numberline {23.5.1}Spektrum vod\IeC {\'\i }ku}{143}{subsection.23.5.1}% 
\contentsline {section}{\numberline {23.6}Modely atomu}{145}{section.23.6}% 
\contentsline {section}{\numberline {23.7}Stimulovan\IeC {\'a} emise}{145}{section.23.7}% 
\contentsline {subsection}{\numberline {23.7.1}Princip laseru}{146}{subsection.23.7.1}% 
\contentsline {chapter}{\numberline {24}Jadern\IeC {\'a} fyzika}{148}{chapter.24}% 
\contentsline {section}{\numberline {24.1}Atomov\IeC {\'e} j\IeC {\'a}dro}{148}{section.24.1}% 
\contentsline {subsection}{\numberline {24.1.1}Silov\IeC {\'e} interakce}{148}{subsection.24.1.1}% 
\contentsline {section}{\numberline {24.2}Vazebn\IeC {\'a} energie j\IeC {\'a}dra}{148}{section.24.2}% 
\contentsline {subsection}{\numberline {24.2.1}Hmotnostn\IeC {\'\i } \IeC {\'u}bytek}{149}{subsection.24.2.1}% 
\contentsline {subsection}{\numberline {24.2.2}Vazebn\IeC {\'a} a separa\IeC {\v c}n\IeC {\'\i } energie}{149}{subsection.24.2.2}% 
\contentsline {section}{\numberline {24.3}Jadern\IeC {\'e} reakce}{150}{section.24.3}% 
\contentsline {subsection}{\numberline {24.3.1}Jadern\IeC {\'a} f\IeC {\'u}ze}{151}{subsection.24.3.1}% 
\contentsline {subsection}{\numberline {24.3.2}Jadern\IeC {\'e} \IeC {\v s}t\IeC {\v e}pen\IeC {\'\i }}{151}{subsection.24.3.2}% 
\contentsline {subsubsection}{Praktick\IeC {\'e} vyu\IeC {\v z}it\IeC {\'\i }}{152}{section*.159}% 
\contentsline {subsection}{\numberline {24.3.3}Transmutace}{153}{subsection.24.3.3}% 
\contentsline {section}{\numberline {24.4}Jadern\IeC {\'e} elektr\IeC {\'a}rny}{153}{section.24.4}% 
\contentsline {subsection}{\numberline {24.4.1}Jadern\IeC {\'y} reaktor}{153}{subsection.24.4.1}% 
\contentsline {chapter}{\numberline {25}Radioaktivita}{155}{chapter.25}% 
\contentsline {section}{\numberline {25.1}Z\IeC {\'a}kladn\IeC {\'\i } informace}{155}{section.25.1}% 
\contentsline {subsection}{\numberline {25.1.1}Historie}{155}{subsection.25.1.1}% 
\contentsline {section}{\numberline {25.2}Druhy radioaktivn\IeC {\'\i }ho z\IeC {\'a}\IeC {\v r}en\IeC {\'\i }}{156}{section.25.2}% 
\contentsline {subsection}{\numberline {25.2.1}Z\IeC {\'a}\IeC {\v r}en\IeC {\'\i } \(\alpha \)}{156}{subsection.25.2.1}% 
\contentsline {subsection}{\numberline {25.2.2}Z\IeC {\'a}\IeC {\v r}en\IeC {\'\i } \(\beta \) }{157}{subsection.25.2.2}% 
\contentsline {subsubsection}{Z\IeC {\'a}\IeC {\v r}en\IeC {\'\i } \(\beta ^{-}\) }{157}{section*.162}% 
\contentsline {subsubsection}{Z\IeC {\'a}\IeC {\v r}en\IeC {\'\i } \(\beta ^ +\) }{157}{section*.163}% 
\contentsline {subsubsection}{Charakteristika z\IeC {\'a}\IeC {\v r}en\IeC {\'\i } \(\beta \) }{157}{section*.164}% 
\contentsline {subsection}{\numberline {25.2.3}Z\IeC {\'a}\IeC {\v r}en\IeC {\'\i } \(\gamma \)}{157}{subsection.25.2.3}% 
\contentsline {section}{\numberline {25.3}Radioaktivita v praxi}{158}{section.25.3}% 
\contentsline {subsection}{\numberline {25.3.1}Vyu\IeC {\v z}it\IeC {\'\i } radioaktivity}{158}{subsection.25.3.1}% 
\contentsline {subsection}{\numberline {25.3.2}Ochrana p\IeC {\v r}ed radioaktivn\IeC {\'\i }m z\IeC {\'a}\IeC {\v r}en\IeC {\'\i }m}{158}{subsection.25.3.2}% 
\contentsline {section}{\numberline {25.4}Rozpadov\IeC {\'e} \IeC {\v r}ady}{158}{section.25.4}% 
\contentsline {section}{\numberline {25.5}Matematick\IeC {\'y} popis radioaktivity}{158}{section.25.5}% 
\contentsline {section}{\numberline {25.6}Urychlova\IeC {\v c}e \IeC {\v c}\IeC {\'a}stic}{160}{section.25.6}% 
\contentsline {section}{\numberline {25.7}Element\IeC {\'a}rn\IeC {\'\i } \IeC {\v c}\IeC {\'a}stice}{161}{section.25.7}% 
\contentsline {subsection}{\numberline {25.7.1}Hadrony}{161}{subsection.25.7.1}% 
\contentsline {paragraph}{Kvarky}{161}{section*.167}% 
\contentsline {subsubsection}{St\IeC {\v r}edn\IeC {\v e} t\IeC {\v e}\IeC {\v z}k\IeC {\'e} \IeC {\v c}\IeC {\'a}stice - Mezony}{161}{section*.168}% 
\contentsline {subsubsection}{Velmi t\IeC {\v e}\IeC {\v z}k\IeC {\'e} \IeC {\v c}\IeC {\'a}stice - Baryony}{161}{section*.169}% 
\contentsline {subsection}{\numberline {25.7.2}Leptony}{161}{subsection.25.7.2}% 
\contentsline {subsection}{\numberline {25.7.3}D\IeC {\v e}len\IeC {\'\i } podle spinu}{162}{subsection.25.7.3}% 
\contentsline {subsubsection}{Fermiony}{162}{section*.170}% 
\contentsline {subsubsection}{Bosony}{162}{section*.171}% 
\contentsline {section}{\numberline {25.8}Detekce \IeC {\v c}\IeC {\'a}stic}{162}{section.25.8}% 
\contentsline {subsection}{\numberline {25.8.1}Geiger-Muller\IeC {\r u}v po\IeC {\v c}\IeC {\'\i }ta\IeC {\v c}}{162}{subsection.25.8.1}% 
